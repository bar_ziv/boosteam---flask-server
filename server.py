from flask import Flask, request, send_file, redirect, url_for, session, g, render_template, abort
import modules.scouting as calc
import json

# set the project root directory as the static folder, you can set others.
app = Flask(__name__, static_url_path='')
teamDict = dict()
data = ''

@app.route('/<number>', methods=['POST'])
def sendTeamData(number):
    if number in teamDict:
        return calc.organizedTeamData(teamDict, number)
    return redirect("teams.html")

@app.route('/getData', methods=['POST'])
def sendData():
    global data
    return json.dumps(data), 200, {'ContentType':'application/json'} # return success (200)

@app.route('/addData.html', methods=['POST'])
def addData():
    global teamDict
    global data
    data = calc.calcPoints(teamDict, request.form['number'], ('land' in request.form), 'touch' in request.form, 'kamea' in request.form, 'mineral' in request.form, request.form['mineralsInSquare'], request.form['mineralsInHigh'], request.form['endgame'])
    print (teamDict)
    return redirect("teams.html")

'''
    return files from the static folder
    only allowed files!!
'''
@app.route('/<path:path>', methods=['POST'])
def send_files(path):
    if(path.endswith(".html")):
        return app.send_static_file(path)
    return redirect('404.html')

@app.route('/', methods=['GET', 'POST'])
def sendLogin():
    return app.send_static_file('teams.html')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

if __name__ == "__main__":
    ip = input("enter ip: ")
    app.run(debug=True, port="80", host=ip)
